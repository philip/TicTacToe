#include <array>

class Board {

public:
    enum Mark {
        Empty,
        Circle,
        Cross,
    };

    Board();

    bool boardFull();

    //! @brief Check if game is running
    //! @return true if game is not finished
    bool running();

    Mark haveWinner();

    //! @param x in range [1, 3]
    //! ...
    bool placeMark(int x, int y, Mark);

    void printBoard();

private:
    using xArr = std::array<Mark, 3>;

    std::array<xArr, 3> m_matrix;
};