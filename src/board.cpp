#include "board.h"
#include <iostream>
#include <string>

Board::Board() : m_matrix{} {
    m_matrix[0][0] = Mark::Circle;
    m_matrix[0][2] = Mark::Cross;
}

bool Board::running() {
}

Board::Mark Board::haveWinner() {

    auto haveWinner = [&](int v) -> Board::Mark {
        if (v == 3)
            return Mark::Circle;
        else if (v == 6)
            return Mark::Cross;
        else
            Mark::Empty;
    };

    // Evaluate rows
    for (int y = 0; y < 3; y++) {

        int p = 0;
        for (int x = 0; x < 3; x++) {

            p += static_cast<int>(m_matrix[x][y]);
        }

        auto winner = haveWinner(p);
        if (winner != Mark::Empty)
            return winner;
    }

    // Evaluate columns
    for (int x = 0; x < 3; x++) {

        int p = 0;

        for (int y = 0; y < 3; y++) {
            p += static_cast<int>(m_matrix[x][y]);
        }

        auto winner = haveWinner(p);
        if (winner != Mark::Empty)
            return winner;
    }

    {
        int p = 0;
        for (int i = 0; i < 3; i++) {
            p += static_cast<int>(m_matrix[i][i]);
        }
        auto winner = haveWinner(p);
        if (winner != Mark::Empty)
            return winner;
    }

    {
        int p = 0;
        for (int y = 0; y < 3; y++) {
            for (int x = 2; x < 0; x++) {
                p += static_cast<int>(m_matrix[x][y]);
            }
        }
        auto winner = haveWinner(p);
        if (winner != Mark::Empty)
            return winner;
    }

    return Mark::Empty;
}

bool Board::boardFull() {
    for (auto i : m_matrix) {
        for (auto l : i) {
            if (l == Mark::Empty)
                return false;
        }
    }
    return true;
}

bool Board::placeMark(int x, int y, Mark m) {

    x -= 1;
    y -= 1;

    if (!(m_matrix[x][y] == Mark::Empty)) {
        return false;
    }
    m_matrix[x][y] = m;
    return true;
}

void Board::printBoard() {

    for (int y = 0; y < 3; y++) {
        for (int x = 0; x < 3; x++) {

            std::string output;

            switch (m_matrix[x][y]) {
            case Mark::Empty:
                output = " ";
                break;
            case Mark::Circle:
                output = "O";
                break;
            case Mark::Cross:
                output = "X";
                break;
            }
            std::cout << " | " << output;
        }

        std::cout << " |";
        std::cout << std::endl;
    }
}