#include "src/board.h"

#include <iostream>

int main(int argc, char** argv) {
    Board b;

    std::cout << "Starting game. Empty Board" << std::endl;
    b.printBoard();

    while (b.running()) {
        for (auto player : {1, 2}) {
            bool inputInvalid = true;
            while (inputInvalid) {

                int x, y;

                std::cout << "Player " << player << " Input play... " << std::endl;
                std::cout << "X pos: " << std::endl;
                std::cin >> x;
                std::cout << "Y pos: " << std::endl;
                std::cin >> y;

                inputInvalid = !b.placeMark(x, y, static_cast<Board::Mark>(player));
                if (inputInvalid)
                    std::cout << "Invalid placement, try again" << std::endl;
                b.printBoard();

                auto winner = b.haveWinner();
                if (winner != Board::Mark::Empty)
                    std::cout << "We have a winner...... Player" << static_cast<int>(player);
            }
        }
    }

    return 0;
}